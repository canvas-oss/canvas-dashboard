# CANVAS
CANVAS Common Application & Network Vulnerability Assessment System is a computer program whose purpose is to to identify and assess known vulnerabilities on an information system.

## CANVAS DASHBOARD
CANVAS DASHBOARD is a component of CANVAS. It includes the Web dashboard used to read assessment results and manage the CANVAS's subsystems configuration.

## License
CANVAS including all its components is distributed under the CeCILL licence. Please see the [LICENSE.CECILL-FR](https://bitbucket.org/canvas-oss/canvas-dashboard/src/master/LICENSE.CECILL-FR) and [LICENCE-CECILL-EN](https://bitbucket.org/canvas-oss/canvas-dashboard/src/master/LICENSE.CECILL-EN) documents for more informations.

## Documentation
The CANVAS general documentation is available at the main [repository's wiki](https://bitbucket.org/canvas-oss/canvas/wiki).
A specific documentation is available on this [repository's wiki](https://bitbucket.org/canvas-oss/canvas-dashboard/wiki).

## Installation steps
### Dependencies
CANVAS DASHBOARD is powered by [Ruby on Rails](http://rubyonrails.org/).
A list of all application dependencies can be found in the [Gemfile](https://bitbucket.org/canvas-oss/canvas-dashboard/src/master/Gemfile) file.

### Steps

## Contribution guidelines

## Contact

### Issues
You can open an issue at the [CANVAS's JIRA Dashboard](https://canvas-oss.atlassian.net/projects/CANVAS/issues). You have to be logged in to open issue.
