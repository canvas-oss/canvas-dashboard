# DASHBOARD DESIGN

## Sitemap

URL | Page | Description
--- | ---- | -----------
RESULTS | |
/ | Homepage | Display statistics about identified hosts and assets and a summary about new assessment results and vulnerabilities.
/summary | Homepage | Display statistics about identified hosts and assets and a summary about new assessment results and vulnerabilities.
/hosts | Hosts | Display a list of all identified hosts and a summary of related assets.
/hosts/:hostid | Host details | Display details about one host. Display a list of identified assets on one host. Display a list of assessment results related to one host. The ```:hostid``` parameter represent a host id in the ```canvas_data``` database.
/hosts/metrics | Hosts metrics configuration | Display CVSS environment metrics parameters and allow to alter this configuration.
/hosts/import | Hosts data import | Display a form to import a list of hosts and its assets.
/assessments | Assessment results | Display a list of all assessment results.
/assessments/CVE-:year-:id-:assetid | Assessment result details | Display details about one assessment result. An assessment is a combinaison of a vulnerability and an asset. That's why ID of both of them are used.
/vulnerabilities | Known vulnerabilities  | Display a list of all known CVE.
/vulnerabilities/CVE-:year-:id | Vulnerability details | Display details about a known CVE. The ```:year``` and ```:id``` parameters represent a standard CVE id including year and number, ex: ```CVE-XXXX-XXXXXX```.
/vulnerabilities/import | Vulnerabilities data import | Display a form to import a list of known vulnerabilities as CVE.
USER MANAGEMENT | |
 | | |
CANVAS SETTINGS | |
 | | |
