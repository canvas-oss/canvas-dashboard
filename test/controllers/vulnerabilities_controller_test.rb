# Copyright 2017-11-25
# Remy BOISSEZON boissezon.remy@gmail.com
# Valentin PRODHOMME valentin@prodhomme.me
# Dylan TROLES chill3d@protonmail.com
# Alexandre ZANNI alexandre.zanni@engineer.com
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
require 'application_system_test_case'

class VulnerabilitiesControllerTest < ApplicationSystemTestCase
  def test_routing
    assert_routing('/vulnerabilities', { controller: 'vulnerabilities', action: 'list' })
    assert_routing('/vulnerabilities/CVE-2017-12345', { controller: 'vulnerabilities', action: 'details', year: '2017', digits: '12345' })
    assert_routing('/vulnerabilities/import', { controller: 'vulnerabilities', action: 'import_form' })
  end
end
