# Copyright 2017-11-22
# Remy BOISSEZON boissezon.remy@gmail.com
# Valentin PRODHOMME valentin@prodhomme.me
# Dylan TROLES chill3d@protonmail.com
# Alexandre ZANNI alexandre.zanni@engineer.com
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

# CveDescription Model
# @author Valentin PRODHOMME
class CveDescription < ApplicationRecord
  # Database identifier
  self.table_name = 'v_cve_description'
  self.primary_key = :id
  # Relations
  belongs_to(:cve, { foreign_key: 'cve' })
  # Requirements
  validates(:cve, { presence: true })
  validates(:lang, { presence: true, length: { minimum: 5, maximum: 5 } })
  validates(:value, { presence: true })
  # Database connection
  establish_connection(:canvas_data)
end
