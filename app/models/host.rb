# Copyright 2017-12-08
# Remy BOISSEZON boissezon.remy@gmail.com
# Valentin PRODHOMME valentin@prodhomme.me
# Dylan TROLES chill3d@protonmail.com
# Alexandre ZANNI alexandre.zanni@engineer.com
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

# Host Model
# @author Valentin PRODHOMME
class Host < ApplicationRecord
  # Database identifier
  self.table_name = 'v_host'
  self.primary_key = :id
  # Relations
  has_many(:asset, { foreign_key: 'host' })
  # Requirements
  validates(:id, { presence: true })
  validates(:name, { length: { minimum: 0, maximum: 64 } })
  validates(:source_date, { presence: true })
  validates(:hostname, { length: { minimum: 0, maximum: 64 } })
  validates(:source_name, { presence: true, length: { minimum: 1, maximum: 64 } })
  # Database connection
  establish_connection(:canvas_data)
end
