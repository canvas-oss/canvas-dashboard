# Rails ApplicationRecord Model base class
class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
end
