# Copyright 2017-11-22
# Remy BOISSEZON boissezon.remy@gmail.com
# Valentin PRODHOMME valentin@prodhomme.me
# Dylan TROLES chill3d@protonmail.com
# Alexandre ZANNI alexandre.zanni@engineer.com
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

# Cve Model
# @author Valentin PRODHOMME
class Cve < ApplicationRecord
  # Database identifier
  self.table_name = 'v_cve'
  self.primary_key = :id
  # Relations
  has_many(:cve_description, { foreign_key: 'cve' })
  has_many(:cve_reference, { foreign_key: 'cve' })
  has_many(:cve_configuration, { foreign_key: 'cve' })
  # Requirements
  validates(:id, { presence: true })
  validates(:year, { presence: true, length: { minimum: 4, maximum: 4 } })
  validates(:digits, { presence: true, length: { minimum: 1, maximum: 32 } })
  validates(:published_date, { presence: true })
  validates(:modified_date, { presence: true })
  validates(:source_date, { presence: true })
  validates(:source_type, { presence: true, length: { minimum: 1, maximum: 16 } })
  validates(:source_format, { presence: true, length: { minimum: 1, maximum: 16 } })
  validates(:source_version, { presence: true, length: { minimum: 1, maximum: 16 } })
  # Database connection
  establish_connection(:canvas_data)
  # Methods
  # Return the CVSSv2 vector string
  def cvssv2_vector
    return "AV:#{cvssv2_av}/AC:#{cvssv2_ac}/Au:#{cvssv2_au}/C:#{cvssv2_c}/I:#{cvssv2_i}/A:#{cvssv2_a}/E:#{cvssv2_e}/RL:#{cvssv2_rl}/RC:#{cvssv2_rc}"
  end

  # Return the CVSSv3 vector string
  def cvssv3_vector
    return "CVSS:3.0/AV:#{cvssv3_av}/AC:#{cvssv3_ac}/PR:#{cvssv3_pr}/UI:#{cvssv3_ui}/S:#{cvssv3_s}/C:#{cvssv3_c}/I:#{cvssv3_i}/A:#{cvssv3_a}/E:#{cvssv3_e}/RL:#{cvssv3_rl}/RC:#{cvssv3_rc}"
  end
end
