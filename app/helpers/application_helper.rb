# Copyright 2017-11-25
# Remy BOISSEZON boissezon.remy@gmail.com
# Valentin PRODHOMME valentin@prodhomme.me
# Dylan TROLES chill3d@protonmail.com
# Alexandre ZANNI alexandre.zanni@engineer.com
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

# ApplicationHelper module
module ApplicationHelper
  # Return a formatted page title if any
  def page_title
    return "CANVAS | #{@page_title}" unless @page_title.nil?
    return 'CANVAS'
  end

  def cvssv2_rating(score)
    return { name: 'High', color: 'red' } if score.between?(7.0, 10.0)
    return { name: 'Medium', color: 'yellow' } if score.between?(4.0, 6.9)
    return { name: 'Low', color: 'blue' } if score.between?(0.0, 3.9)
    return { name: 'None', color: 'grey' }
  end

  def cvssv3_rating(score)
    return { name: 'Critical', color: 'red' } if score.between?(9.0, 10.0)
    return { name: 'High', color: 'orange' } if score.between?(7.0, 8.9)
    return { name: 'Medium', color: 'yellow' } if score.between?(4.0, 6.9)
    return { name: 'Low', color: 'blue' } if score.between?(0.1, 3.9)
    return { name: 'None', color: 'grey' }
  end

  def asset_logo(name)
    return { icon: 'windows' } if name.include? 'windows'
    return { icon: 'linux' } if name.include? 'linux'
    return { icon: 'apple' } if name.include? 'iphone_os'
    return { icon: 'apple' } if name.include? 'mac_os'
    return { icon: 'apple' } if name.include? 'apple_tv'
    return { icon: 'cube' }
  end

  def asset_type(wfn_part)
    return 'Hardware component' if wfn_part == 'h'
    return 'Operating system' if wfn_part == 'o'
    return 'Software application' if wfn_part == 'a'
    return wfn_part
  end
end
