# Copyright 2017-11-25
# Remy BOISSEZON boissezon.remy@gmail.com
# Valentin PRODHOMME valentin@prodhomme.me
# Dylan TROLES chill3d@protonmail.com
# Alexandre ZANNI alexandre.zanni@engineer.com
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
require 'cvss_suite'

# Vulnerabilities controller
# @author Valentin PRODHOMME
class VulnerabilitiesController < ApplicationController
  LIST_ITEM_PER_PAGE = 25
  # Struct holding vulnerability informations
  Vulnerability = Struct.new(:cve, :descriptions, :references, :products, :cvssv2, :cvssv3)
  # List all known CVE
  def list
    # define page title
    @page_title = 'Vulnerabilities'
    # Validate userdata
    # Page must be an integer
    if !params[:page].nil? && params[:page].integer?
      page = params[:page]
    else
      page = 1
    end
    # Get a list of vulnerabilities
    @cve_list = Cve.all.paginate({ page: page, per_page: LIST_ITEM_PER_PAGE }).order('modified_date DESC')
    # Prepare the list for view
    @vulnerabilities = []
    @cve_list.each do |cve|
      vulnerability = Vulnerability.new(cve, nil, nil, nil, nil)
      # Setup CVSS vectors
      begin
        vulnerability.cvssv2 = CvssSuite.new(cve.cvssv2_vector)
      rescue RuntimeError, ArgumentError
        vulnerability.cvssv2 = nil
      end
      begin
        vulnerability.cvssv3 = CvssSuite.new(cve.cvssv3_vector)
      rescue RuntimeError, ArgumentError
        vulnerability.cvssv3 = nil
      end
      @vulnerabilities << vulnerability
    end
    # Render
  end

  # Show details about a CVE entry
  def details
    @page_title = "CVE-#{params[:year]}-#{params[:digits]}"
    # Define vulnerability
    @vulnerability = Vulnerability.new(nil, nil, nil, nil, nil)
    # Get vulnerability informations
    @vulnerability.cve = Cve.where({ year: params[:year], digits: params[:digits] }).first
    # Get vulnerability descriptions
    @vulnerability.descriptions = CveDescription.where({ cve: @vulnerability.cve.id })
    # Get vulnerability references
    @vulnerability.references = CveReference.where({ cve: @vulnerability.cve.id })
    # Setup CVSS vectors
    begin
      @vulnerability.cvssv2 = CvssSuite.new(@vulnerability.cve.cvssv2_vector)
    rescue RuntimeError, ArgumentError
      @vulnerability.cvssv2 = nil
    end
    begin
      @vulnerability.cvssv3 = CvssSuite.new(@vulnerability.cve.cvssv3_vector)
    rescue RuntimeError, ArgumentError
      @vulnerability.cvssv3 = nil
    end
    # Render
  end

  # Guide user in order to import CVE database file
  def import_form
    @page_title = 'Import CVE list'
    # Render
  end

  # Show result after an import
  def import_result
    @page_title = 'Import CVE list'
    # Render
  end
end
