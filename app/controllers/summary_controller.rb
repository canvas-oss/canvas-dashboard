# Copyright 2017-11-25
# Remy BOISSEZON boissezon.remy@gmail.com
# Valentin PRODHOMME valentin@prodhomme.me
# Dylan TROLES chill3d@protonmail.com
# Alexandre ZANNI alexandre.zanni@engineer.com
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

# Hosts controller
# @author Dylan TROLES
class SummaryController < ApplicationController
  ListScore = Struct.new(:unclassified, :low, :medium, :high, :critical)
  def home
    @page_title = 'Summary'
    @list = ListScore.new(nil, nil, nil, nil, nil)
    @list.unclassified = Assessment.where({ cvssv3_score: 0.0 }).count
    @list.low = Assessment.where({ cvssv3_score: 0.1..3.9 }).count
    @list.medium = Assessment.where({ cvssv3_score: 4.0..6.9 }).count
    @list.high = Assessment.where({ cvssv3_score: 7.0..8.9 }).count
    @list.critical = Assessment.where({ cvssv3_score: 9.0..10.0 }).count
    @assessments_stats_number = @list.unclassified + @list.low + @list.medium + @list.high + @list.critical
    @vulnerabilities_stats_number = Cve.count
    @hosts_stats_number = Host.count
    @data = { "Unclassified": @list.unclassified, "Low": @list.low, "Medium": @list.medium, "High": @list.high, "Critical": @list.critical }
    @options = {
      donut: true,
      colors: %w[#808080 #2185d0 #fbbd08 #f2711c #db2828],
      library: {
        plugins: {
          datalabels: {
            anchor: 'start',
            backgroundColor: %w[#808080 #2185d0 #fbbd08 #f2711c #db2828],
            borderColor: 'white',
            borderRadius: 25,
            borderWidth: 2,
            color: 'white',
            font: {
              weight: 'bold'
            }
          }
        }
      }
    }
  end
end
