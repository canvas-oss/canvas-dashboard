# Copyright 2017-11-25
# Remy BOISSEZON boissezon.remy@gmail.com
# Valentin PRODHOMME valentin@prodhomme.me
# Dylan TROLES chill3d@protonmail.com
# Alexandre ZANNI alexandre.zanni@engineer.com
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
require 'cvss_suite'

# Assessments controller
# @author Dylan TROLES
class AssessmentsController < ApplicationController
  LIST_ITEM_PER_PAGE = 25
  AssessmentStruct = Struct.new(:assessment, :asset, :cve, :host, :date, :descriptions, :cvssv2, :cvssv3)
  # List all assessments
  def list
    # define page title
    @page_title = 'Assessments'
    # Validate userdata
    # Page must be an integer
    if !params[:page].nil? && params[:page].integer?
      page = params[:page]
    else
      page = 1
    end
    # Get a list of assessments
    @assessment_list = Assessment.all.paginate({ page: page, per_page: LIST_ITEM_PER_PAGE }).order('date DESC')
    # Prepare the list for view
    @assessments = []
    @assessment_list.each do |assessment|
      assessment_item = AssessmentStruct.new(nil, nil, nil, nil, nil, nil, nil, nil)
      assessment_item.assessment = assessment
      assessment_item.cve = Cve.where({ id: assessment_item.assessment.cve }).first
      assessment_item.asset = Asset.where({ id: assessment_item.assessment.asset }).first
      assessment_item.host = Host.where({ id: assessment_item.assessment.host }).first
      begin
        assessment_item.cvssv2 = CvssSuite.new(assessment_item.cve.cvssv2_vector)
      rescue RuntimeError, ArgumentError
        assessment_item.cvssv2 = nil
      end
      begin
        assessment_item.cvssv3 = CvssSuite.new(assessment_item.cve.cvssv3_vector)
      rescue RuntimeError, ArgumentError
        assessment_item.cvssv3 = nil
      end
      @assessments << assessment_item
    end
    @data = { "2017-12-10": 32, "2017-12-12": 27, "2017-12-14": 29, "2017-12-16": 25, "2017-12-18": 26 }
    @options = {
      colors: %w[purple],
      download: true,
      library: {
        plugins: {
          datalabels: {
            backgroundColor: 'purple',
            borderColor: 'white',
            borderRadius: 25,
            borderWidth: 2,
            color: 'white',
            font: {
              weight: 'bold'
            }
          }
        }
      }
    }
    @data_subnetwork = [
      { name: 'Low', data: { "2017-12-10": 8, "2017-12-12": 5, "2017-12-14": 6, "2017-12-16": 7, "2017-12-18": 6 } },
      { name: 'Medium', data: { "2017-12-10": 7, "2017-12-12": 9, "2017-12-14": 8, "2017-12-16": 6, "2017-12-18": 5 } },
      { name: 'High', data: { "2017-12-10": 5, "2017-12-12": 2, "2017-12-14": 4, "2017-12-16": 7, "2017-12-18": 5 } },
      { name: 'Critical', data: { "2017-12-10": 2, "2017-12-12": 1, "2017-12-14": 0, "2017-12-16": 4, "2017-12-18": 2 } }
    ]
    @options_subnetwork = {
      colors: %w[green yellow orange red],
      download: true,
      library: {
        plugins: {
          datalabels: {
            display: false
          }
        }
      }
    }
  end

  # Show details about an assessment entry
  def details
    @page_title = "Details - Assessment #{params[:year]}-#{params[:digits]}-#{params[:id]}"
    @assessment = AssessmentStruct.new(nil, nil, nil, nil, nil, nil, nil, nil)
    @assessment.assessment = Assessment.where({ asset: params[:id], year: params[:year], digits: params[:digits] }).first
    @assessment.cve = Cve.where({ id: @assessment.assessment.cve }).first
    @assessment.asset = Asset.where({ id: @assessment.assessment.asset }).first
    @assessment.host = Host.where({ id: @assessment.host }).first
    @assessment.descriptions = CveDescription.where({ cve: @assessment.cve.id })
    begin
      @assessment.cvssv2 = CvssSuite.new(@assessment.cve.cvssv2_vector)
    rescue RuntimeError, ArgumentError
      @assessment.cvssv2 = nil
    end
    begin
      @assessment.cvssv3 = CvssSuite.new(@assessment.cve.cvssv3_vector)
    rescue RuntimeError, ArgumentError
      @assessment.cvssv3 = nil
    end

    # Render
  end
end
