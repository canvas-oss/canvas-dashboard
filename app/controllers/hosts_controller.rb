# Copyright 2017-11-25
# Remy BOISSEZON boissezon.remy@gmail.com
# Valentin PRODHOMME valentin@prodhomme.me
# Dylan TROLES chill3d@protonmail.com
# Alexandre ZANNI alexandre.zanni@engineer.com
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

# Hosts controller
# @author Dylan TROLES
class HostsController < ApplicationController
  LIST_ITEM_PER_PAGE = 25
  # Struct holding vulnerability informations
  HostItem = Struct.new(:host, :assets_os, :assets, :assessments)
  # List all hosts
  def list
    # define page title
    @page_title = 'Hosts'
    # Validate userdata
    # Page must be an integer
    if !params[:page].nil? && params[:page].integer?
      page = params[:page]
    else
      page = 1
    end
    # Get a list of hosts
    @host_list = Host.all.paginate({ page: page, per_page: LIST_ITEM_PER_PAGE }).order('id ASC')
    @hosts = []
    @host_list.each do |host|
      host_item = HostItem.new(host, nil, nil)
      host_item.assets_os = Asset.where({ host: host.id, wfn_part: 'o' })
      @hosts << host_item
    end
    # Get statistics
    @count_agents = Host.count
    @count_assets = Asset.count
    @last_time_agent = Date.parse(Asset.order('source_date DESC').first.source_date.to_s)
    # Render
  end

  # Show details about a host entry
  def details
    @page_title = "Host #{params[:id]}"
    # Define host
    @host = HostItem.new(nil, nil, nil)
    # Get host informations
    @host.host = Host.where({ id: params[:id] }).first
    # Get host assets list
    @host.assets_os = Asset.where({ host: @host.host.id, wfn_part: 'o' })
    @host.assets = Asset.where({ host: @host.host.id })
    @host.assessments = Assessment.where({ host: @host.host.id })

    @data = { "Low": 1, "Medium": 1, "High": 1, "Critical": 1 }
    @options = {
      donut: true,
      colors: %w[#2185d0 #fbbd08 #f2711c #db2828],
      library: {
        plugins: {
          datalabels: {
            anchor: 'start',
            backgroundColor: %w[#2185d0 #fbbd08 #f2711c #db2828],
            borderColor: 'white',
            borderRadius: 25,
            borderWidth: 2,
            color: 'white',
            font: {
              weight: 'bold'
            }
          }
        }
      }
    }
    # Render
  end

  # Guide user in order to import host database file
  def import_form
    @page_title = 'Import'
  end

  def import_result
    @page_title = 'Import'
  end

  def metrics_form
    @page_title = 'Metrics'
  end

  def metrics_result
    @page_title = 'Metrics'
  end
end
