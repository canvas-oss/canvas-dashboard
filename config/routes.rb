# Copyright 2017-11-25
# Remy BOISSEZON boissezon.remy@gmail.com
# Valentin PRODHOMME valentin@prodhomme.me
# Dylan TROLES chill3d@protonmail.com
# Alexandre ZANNI alexandre.zanni@engineer.com
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
# @author Dylan TROLES, Valentin PRODHOMME

Rails.application.routes.draw do
  # Summary route
  get('/summary', { to: 'summary#home' })
  # Vulnerabilities related routes
  get('/vulnerabilities', { to: 'vulnerabilities#list' })
  get('/vulnerabilities/CVE-:year-:digits', { to: 'vulnerabilities#details', constraints: { year: /\d+/, digits: /\d+/ } })
  get('/vulnerabilities/import', { to: 'vulnerabilities#import_form' })
  post('/vulnerabilities/import', { to: 'vulnerabilities#import_result' })
  # Hosts related routes
  get('/hosts', { to: 'hosts#list' })
  get('/hosts/:id', { to: 'hosts#details', constraints: { id: /\d+/ } })
  get('/hosts/import', { to: 'hosts#import_form' })
  post('/hosts/import', { to: 'hosts#import_result' })
  get('/hosts/metrics', { to: 'hosts#metrics_form' })
  post('/hosts/metrics', { to: 'hosts#metrics_result' })
  # Assessments related routes
  get('/assessments', { to: 'assessments#list' })
  get('/assessments/:year-:digits-:id', { to: 'assessments#details', constraints: { year: /\d+/, digits: /\d+/, id: /\d+/ } })
  # Root
  root({ to: 'summary#home' })
end
