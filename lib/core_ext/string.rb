# Copyright 2017-11-25
# Remy BOISSEZON boissezon.remy@gmail.com
# Valentin PRODHOMME valentin@prodhomme.me
# Dylan TROLES chill3d@protonmail.com
# Alexandre ZANNI alexandre.zanni@engineer.com
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

# Extends String class
# Add some utilities to manipulate a String object
# @author Valentin PRODHOMME <valentin@prodhomme.me>
class String
  # Check if the content of String can represent a valid Float.
  # @return [Boolean] +true+ if the content of String can represent a Float or +false+.
  # @example :
  # "2".float? # => true
  # "abc".float? # => false
  # "1.333".float? # => true
  def float?
    true if Float self
  rescue TypeError, ArgumentError
    false
  end

  # Check if the content of String can represent a valid Integer.
  # @return [Boolean] +true+ if the content of String can represent an Integer or +false+.
  # @example :
  # "7".integer? # => true
  # "abc".integer? # => false
  # "7.1".integer? # => false
  def integer?
    true if Integer self
  rescue TypeError, ArgumentError
    false
  end
end
